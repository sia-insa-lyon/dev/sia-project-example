[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)
# SIA Projet Exemple

Example of GitLab project to follow for new projects. This repo can be forked. This project is constitued of :
* A contributing template
* A license template
* A changelog template
* Folders for project example :
  * Agnostic template folder with every important ``md`` template files and a guide to create a proper project
  * Basic projects to be ready to use immediatly for a new project (for now, only basic React, Django and Spring Boot project will be present)

## Contributing
This is an open source project and we are very happy to accept community contributions. Please refer to [Contributing to GitLab page](https://about.gitlab.com/contributing/) for more details.

After checking [GitLab Issues](https://gitlab.com/sia-insa-lyon/sia-projet-exemple/issues), feel free to contribute by sending your pull requests.
Please refer to [CONTRIBUTING](CONTRIBUTING.md) file to have more details.

## Licence
See the [LICENSE](LICENSE) file for licensing information as it pertains to
files in this repository.
