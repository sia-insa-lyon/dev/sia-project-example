# SIA Project Example changelog

The latest version of this file can be found at the master branch of the
sia-projet-exemple repository.

## 1.0.0

### Removed (0 change)


### Fixed (1 changes, 0 of them are from the community)

- Correct README.md

### Changed (0 change)

- Protect master against direct changes

### Added (5 changes)

- Adding README.md
- Adding CONTRIBUTING.md
- Adding CHANGELOG.md
- Adding tags

### Other (0 change)